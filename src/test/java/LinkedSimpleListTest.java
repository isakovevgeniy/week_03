import org.junit.Assert;
import org.junit.Test;
import ru.edu.LinkedSimpleList;

public class LinkedSimpleListTest {
    /**
     * linkedSimpleList - экземпляр тестируемого класса.
     */
    private LinkedSimpleList<String> linkedList = new LinkedSimpleList<>();

    /**
     * клличество элементов очереди в тестовом экземпляре.
     */
    private final int testSize = 3;

    /**
     * Тест добавления элементов в конец списка.
     * проверяем увеличение количества. метод size().
     * проверяем наличие добавленного элемента. метод get().
     */
    @Test
    public void addTest() {
        Assert.assertEquals(0, linkedList.size());

        linkedList.add("first");
        Assert.assertEquals(1, linkedList.size());
        Assert.assertEquals("first", linkedList.get(0));

        linkedList.add("second");
        Assert.assertEquals(2, linkedList.size());
        Assert.assertEquals("second", linkedList.get(1));

        linkedList.add("third");
        Assert.assertEquals(testSize, linkedList.size());
        Assert.assertEquals("third", linkedList.get(2));
    }

    /**
     * Тест установления значения элемента по индексу.
     */
    @Test
    public void setTest() {
        addTest();
        linkedList.set(0, "otherFirst");
        Assert.assertEquals("otherFirst", linkedList.get(0));
        linkedList.set(1, "otherSecond");
        Assert.assertEquals("otherSecond", linkedList.get(1));
        linkedList.set(2, "otherThird");
        Assert.assertEquals("otherThird", linkedList.get(2));
    }

    /**
     * Тест получения значения элемента по индексу.
     */
    @Test
    public void getTest() {
        addTest();
        Assert.assertEquals("first", linkedList.get(0));
        Assert.assertEquals("second", linkedList.get(1));
        Assert.assertEquals("third", linkedList.get(2));
    }

    /**
     * Тест удаления элемента.
     */
    @Test
    public void removeTest() {
        addTest();
        linkedList.remove(1);
        Assert.assertEquals("first", linkedList.get(0));
        Assert.assertEquals("third", linkedList.get(1));
        linkedList.remove(0);
        Assert.assertEquals("third", linkedList.get(0));
    }

    /**
     * Тест получения индекса элемента по значению.
     */
    @Test
    public void indexOfTest() {
        addTest();
        Assert.assertEquals(0, linkedList.indexOf("first"));
        Assert.assertEquals(1, linkedList.indexOf("second"));
        Assert.assertEquals(2, linkedList.indexOf("third"));
        Assert.assertEquals(-1, linkedList.indexOf("fourth"));
    }

    /**
     * Тест возвращения и изменения размера списка.
     */
    @Test
    public void sizeTest() {
        int size = testSize;
        addTest();
        Assert.assertEquals(size, linkedList.size());

        linkedList.remove(0);
        size--;
        Assert.assertEquals(size, linkedList.size());

        linkedList.remove(0);
        size--;
        Assert.assertEquals(size, linkedList.size());

        linkedList.remove(0);
        size--;
        Assert.assertEquals(size, linkedList.size());

        linkedList.add("first");
        size++;
        Assert.assertEquals(size, linkedList.size());
    }

    /**
     * Тест выброса исключения при введении неверного индекса.
     */
    @Test(expected = IndexOutOfBoundsException.class)
    public void checkIndexTest() {
        linkedList.checkIndex(testSize + 1);
    }
}
