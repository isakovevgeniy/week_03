import org.junit.Assert;
import org.junit.Test;
import ru.edu.ArraySimpleQueue;

public class ArraySimpleQueueTest {

    /**
     * arraySimpleQueue -экземпляр тестируемого класса.
     */
    private ArraySimpleQueue<String> arrayQueue = new ArraySimpleQueue<>(10);

    /**
     * клличество элементов очереди в тестовом экземпляре.
     */
    private final int testSize = 10;

    /**
     * тест добавления объекта в очередь.
     */
    @Test
    public void offerTest() {
        int capacity = arrayQueue.capacity();
        Assert.assertTrue(arrayQueue.offer("String 0"));
        for (int i = 1; i < capacity; i++) {
            arrayQueue.offer("String " + i);
        }
        Assert.assertFalse(arrayQueue.offer("String"));
    }

    /**
     * Тест возвращения первого объекта из очереди с удалением.
     */
    @Test
    public void pollTest() {
        Assert.assertNull(arrayQueue.poll());
        Assert.assertEquals(0, arrayQueue.size());
        offerTest();
        int size = arrayQueue.size();
        Assert.assertEquals("String 0", arrayQueue.poll());
        Assert.assertEquals("String 1", arrayQueue.poll());
        Assert.assertEquals(size - 2, arrayQueue.size());
    }

    /**
     * Тест возвращения первого объекта из очереди без удаления.
     */
    @Test
    public void peekTest() {
        offerTest();
        int size = arrayQueue.size();
        Assert.assertEquals("String 0", arrayQueue.peek());
        Assert.assertEquals("String 0", arrayQueue.peek());
        Assert.assertEquals(size, arrayQueue.size());
    }

    /**
     * Тест возвращения колшичества элементов очереди.
     */
    @Test
    public void sizeTest() {
        Assert.assertEquals(0, arrayQueue.size());
        offerTest();
        Assert.assertEquals(testSize, arrayQueue.size());
        Assert.assertEquals("String 0", arrayQueue.poll());
        Assert.assertEquals(testSize - 1, arrayQueue.size());
    }

    /**
     * Тест возвращения доступного места в очереди.
     */
    @Test
    public void capacityTest() {
        Assert.assertEquals(testSize, arrayQueue.capacity());
    }
}

