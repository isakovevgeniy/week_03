import org.junit.Assert;
import org.junit.Test;
import ru.edu.LinkedSimpleQueue;


public class LinkedSimpleQueueTest {
    /**
     * test - Экземполяр тестируемого класса.
     */
    private LinkedSimpleQueue<String> test = new LinkedSimpleQueue<>();

    /**
     * клличество элементов очереди в тестовом экземпляре.
     */
    private final int testSize = 3;

    /**
     * Тест добавления элеменов в конец очереди.
     * проверка увеличени яразмера очереди
     */
    @Test
    public void offerTest() {
        Assert.assertEquals(0, test.size());

        test.offer("first");
        Assert.assertEquals(1, test.size());
        test.offer("second");
        Assert.assertEquals(2, test.size());
        test.offer("third");
        Assert.assertEquals(testSize, test.size());
    }

    /**
     * Тест получения первого элемента с удалением.
     * получаем элемент
     * размер уменьшается на 1
     */
    @Test
    public void pollTest() {
        offerTest();
        Assert.assertEquals("first", test.poll());
        Assert.assertEquals(testSize - 1, test.size());
    }

    /**
     * Тест получения первого элемента без удаления.
     * получаем элемент размер коллекции сохряняется.
     */
    @Test
    public void peekTest() {
        offerTest();
        Assert.assertEquals("first", test.peek());
        Assert.assertEquals(testSize, test.size());
    }

    /**
     * Тест получения фактического размера очереди.
     */
    @Test
    public void sizeTest() {
        offerTest();
        Assert.assertEquals(testSize, test.size());
    }

    /**
     * Тест получения доступного размера очереди.
     * -1 если очередь бесконечная
     */
    @Test
    public void capacityTest() {
        Assert.assertEquals(-1, test.capacity());
    }
}

