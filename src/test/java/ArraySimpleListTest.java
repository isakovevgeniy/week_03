import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.edu.ArraySimpleList;

public class ArraySimpleListTest {

    /**
     * arraySimpleList - тестовый экземпляр.
     */
    private ArraySimpleList<String> arraySimpleList = new ArraySimpleList<>(4);

    /**
     * клличество элементов очереди в тестовом экземпляре.
     */
    private final int testSize = 3;

    /**
     * Тестовые данные.
     */
    @Before
    public void initiate() {
        arraySimpleList.add("firstString");
        arraySimpleList.add("secondString");
        arraySimpleList.add("thirdString");
    }

    /**
     * тает добавления данных.
     */
    @Test
    public void addTest() {
        int sizeBeforeAdd = arraySimpleList.size();
        arraySimpleList.add("fourthString");
        Assert.assertEquals(++sizeBeforeAdd, arraySimpleList.size());
        arraySimpleList.add("fifthString");
        Assert.assertEquals(++sizeBeforeAdd, arraySimpleList.size());
    }

    /**
     * тест установки значения элемента по индексу.
     */
    @Test
    public void setTest() {
        Assert.assertEquals("secondString", arraySimpleList.get(1));
        arraySimpleList.set(1, "otherString");
        Assert.assertEquals("otherString", arraySimpleList.get(1));
        try {
            arraySimpleList.set(testSize, "otherString");
            Assert.fail();
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("ArrayIndexOutOfBoundsException caught");
        }
    }

    /**
     * тест получения элемента по индексу.
     */
    @Test
    public void getTest() {
        Assert.assertEquals("firstString", arraySimpleList.get(0));
        Assert.assertEquals("secondString", arraySimpleList.get(1));
        Assert.assertEquals("thirdString", arraySimpleList.get(2));
    }

    /**
     * тест удаления элемента.
     */
    @Test
    public void removeTest() {
        arraySimpleList.remove(1);
        Assert.assertEquals(testSize - 1, arraySimpleList.size());
        Assert.assertEquals("thirdString", arraySimpleList.get(1));

        arraySimpleList.remove(0);
        Assert.assertEquals(testSize - 2, arraySimpleList.size());
        Assert.assertEquals("thirdString", arraySimpleList.get(0));

        arraySimpleList.remove(0);
        Assert.assertEquals(0, arraySimpleList.size());
    }

    /**
     * тест получения индекса по значению.
     */
    @Test
    public void indexOfTest() {
        Assert.assertEquals(0, arraySimpleList.indexOf("firstString"));
        Assert.assertEquals(1, arraySimpleList.indexOf("secondString"));
        Assert.assertEquals(2, arraySimpleList.indexOf("thirdString"));
        Assert.assertEquals(-1, arraySimpleList.indexOf("otherString"));
    }

    /**
     * тест получения размера/ количества элементов.
     */
    @Test
    public void sizeTest() {
        int size = testSize;
        Assert.assertEquals(size, arraySimpleList.size());

        arraySimpleList.remove(0);
        size = size - 1;
        Assert.assertEquals(size, arraySimpleList.size());

        arraySimpleList.remove(0);
        arraySimpleList.remove(0);
        size = size - 2;
        Assert.assertEquals(size, arraySimpleList.size());

        arraySimpleList.add("smth");
        size = size + 1;
        Assert.assertEquals(size, arraySimpleList.size());
    }

    /**
     * Тест выброса исключения при введении индекса >= size.
     */
    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void checkIndexTest_1() {
        arraySimpleList.checkIndex(testSize + 1);
    }

    /**
     * Тест выброса исключения при введении индекса < 0.
     */
    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void checkIndexTest_2() {
        arraySimpleList.checkIndex(testSize-testSize-1);
    }
}
