package ru.edu;

public class LinkedSimpleQueue<T> implements SimpleQueue<T> {

    /**
     * size - фактический размер/ количество элементов.
     */
    private int size;

    /**
     * last - head. начальный элемент
     */
    private SimpleItem<T> last;

    /**
     * size - tail. конечноый элемент.
     */
    private SimpleItem<T> first;

    private static class SimpleItem<T> {
        /**
         * previous - пердыдущий элемент списка.
         */
        private SimpleItem<T> previous;

        /**
         * value - значение.
         */
        private T value;
        /**
         * next - следующий элемент списка.
         */
        private SimpleItem<T> next;

        SimpleItem(final SimpleItem<T> prev,
                   final T val,
                   final SimpleItem<T> nxt) {
            previous = prev;
            value = val;
            next = nxt;
        }

        public SimpleItem<T> getPrevious() {
            return previous;
        }

        public T getValue() {
            return value;
        }

        public SimpleItem<T> getNext() {
            return next;
        }

        public void setPrevious(final SimpleItem<T> prev) {
            previous = prev;
        }

        public void setNext(final SimpleItem<T> nxt) {
            next = nxt;
        }

        public void remove() {
            getNext().getNext().setPrevious(this);
            setNext(getNext().getNext());
        }
    }

    /**
     * Добавление элемента в конец очереди.
     *
     * @param value элемент
     * @return true - если удалось поместить элемент (если есть место в очереди)
     */
    @Override
    public boolean offer(final T value) {
        if (size == 0) {
            SimpleItem<T> element = new SimpleItem<T>(first, value, last);
            first = new SimpleItem<T>(null, null, element);
            last = new SimpleItem<T>(element, null, null);
        } else {
            SimpleItem<T> element;
            element = new SimpleItem<T>(last.getPrevious(), value, last);
            last.getPrevious().setNext(element);
            last.setPrevious(element);
        }
        size++;
        return true;
    }

    /**
     * Получение и удаление первого элемента из очереди.
     *
     * @return первый элемент из очереди
     */
    @Override
    public T poll() {
        T value = first.getNext().getValue();
        first.remove();
        size--;
        return value;
    }

    /**
     * Получение БЕЗ удаления первого элемента из очереди.
     *
     * @return первый элемент из очереди
     */
    @Override
    public T peek() {
        return first.getNext().getValue();
    }

    /**
     * Количество элементов в очереди.
     *
     * @return количество элементов
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * Количество элементов которое может уместиться в очереди.
     *
     * @return -1 если не ограничено (будет расширяться),
     * либо конкретное число если ограничено.
     */
    @Override
    public int capacity() {
        return -1;
    }
}
