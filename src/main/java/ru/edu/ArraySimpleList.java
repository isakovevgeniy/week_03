package ru.edu;

public class ArraySimpleList<T> implements SimpleList<T> {

    /**
     * ARRAY_LENGTH - длина массива.
     */
    private final int arrayLength;
    /**
     * array - это массив.
     */
    private Object[] array;

    /**
     * size - фактический размер/ количество элементов.
     */
    private int size;

    /**
     * @param length - длина списка.
     */
    public ArraySimpleList(final int length) {
        arrayLength = length;
        array = new Object[arrayLength];
    }

    /**
     * Добавление элемента в конец списка.
     *
     * @param value элемент
     */
    @Override
    public void add(final T value) {
        if (size == array.length) {
            Object[] temp = array;
            array = new Object[size + arrayLength];
            for (int i = 0; i < size; i++) {
                array[i] = temp[i];
            }
        }
        array[size++] = value;
    }

    /**
     * Установка значения элемента по индексу.
     *
     * @param index индекс
     * @param value элемент
     */
    @Override
    public void set(final int index, final T value) {
        if (index >= size) {
            throw new ArrayIndexOutOfBoundsException("index not found");
        }
        array[index] = value;
    }

    /**
     * Получение элемента из списка.
     *
     * @param index индекс
     * @return значение элемента или null
     */
    @Override
    @SuppressWarnings("unchecked")
    public T get(final int index) {
        checkIndex(index);
        return (T) array[index];
    }

    /**
     * Удаление элемента по индексу.
     * При удалении происходит сдвиг элементов влево, начиная с index+1 и далее.
     *
     * @param index индекс
     */
    @Override
    public void remove(final int index) {
        checkIndex(index);
        if (size == 1) {
            array = new Object[arrayLength];
        } else {
            for (int i = index + 1; i < size; i++) {
                array[i - 1] = array[i];
            }
            array[size - 1] = null;
        }
        size--;
    }

    /**
     * Получение индекса элемента по его значению.
     *
     * @param value элемент
     * @return index индекс элемента
     */
    @Override
    public int indexOf(final T value) {
        int index = -1;
        for (int i = 0; i < size; i++) {
            if (array[i].equals(value)) {
                return i;
            }
        }
        return index;
    }

    /**
     * Получение размера списка(количество элементов).
     *
     * @return размер списка
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * Проверка наличия правильности индекса(наличия его в списке).
     * @param index индекс
     */
    public void checkIndex(final int index) {
        if (index >= size || index < 0) {
            throw new ArrayIndexOutOfBoundsException("index not found");
        }
    }
}
