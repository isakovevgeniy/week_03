package ru.edu;

public class LinkedSimpleList<T> implements SimpleList<T> {

    /**
     * size - фактический размер/ количество элементов.
     */
    private int size;

    /**
     * last - head. начальный элемент.
     */
    private Element<T> last;

    /**
     * size - tail. конечноый элемент.
     */
    private Element<T> first;

    private static class Element<T> {

        /**
         * previous - пердыдущий элемент списка.
         */
        private Element<T> previous;
        /**
         * value - значение.
         */
        private T value;
        /**
         * next - следующий элемент списка.
         */
        private Element<T> next;

        Element(final Element<T> prev,
                final T val,
                final Element<T> nxt) {
            previous = prev;
            value = val;
            next = nxt;
        }

        public Element<T> getPrevious() {
            return previous;
        }

        public T getValue() {
            return value;
        }

        public Element<T> getNext() {
            return next;
        }

        public void setPrevious(final Element<T> prev) {
            previous = prev;
        }

        public void setValue(final T val) {
            value = val;
        }

        public void setNext(final Element<T> nxt) {
            next = nxt;
        }

        public void set(final int index, final T val) {
            if (index == 0) {
                getNext().setValue(val);
            } else {
                getNext().set(index - 1, val);
            }
        }

        public T get(final int index) {
            if (index == 0) {
                return getNext().getValue();
            } else {
                return getNext().get(index - 1);
            }
        }

        public void remove(final int index) {
            if (index == 0) {
                getNext().getNext().setPrevious(this);
                setNext(getNext().getNext());
            } else {
                getNext().remove(index - 1);
            }
        }

        public int indexOf(final T val, final int size) {
            int count = -1;
            Element<T> element = this.getNext();
            for (int i = 0; i <= size; i++) {
                if (i == size) {
                    break;
                }
                if (element.getValue().equals(val)) {
                    count = i;
                    break;
                } else {
                    element = element.getNext();
                }
            }
            return count;
        }
    }


    /**
     * Добавление элемента в конец списка.
     *
     * @param value элемент
     */
    @Override
    public void add(final T value) {
        if (size == 0) {
            Element<T> element = new Element<>(first, value, last);
            first = new Element<>(null, null, element);
            last = new Element<>(element, null, null);
        } else {
            Element<T> element;
            element = new Element<>(last.getPrevious(), value, last);
            last.getPrevious().setNext(element);
            last.setPrevious(element);
        }
        size++;
    }

    /**
     * Установка значения элемента по индексу.
     *
     * @param index индекс
     * @param value элемент
     */
    @Override
    public void set(final int index, final T value) {
        checkIndex(index);
        first.set(index, value);
    }

    /**
     * Получение элемента из списка.
     *
     * @param index индекс
     * @return значение элемента или null
     */
    @Override
    public T get(final int index) {
        checkIndex(index);
        return first.get(index);
    }

    /**
     * Удаление элемента по индексу.
     * При удалении происходит сдвиг элементов влево, начиная с index+1 и далее.
     *
     * @param index индекс
     */
    @Override
    public void remove(final int index) {
        checkIndex(index);
        first.remove(index);
        size--;
    }

    /**
     * Получение индекса элемента по его значению.
     *
     * @param value элемент
     * @return int
     */
    @Override
    public int indexOf(final T value) {
        return first.indexOf(value, size);
    }

    /**
     * Получение размера списка(количество элементов).
     *
     * @return размер списка
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * Проверка наличия правильности индекса(наличия его в списке).
     * @param index индекс
     */
    public void checkIndex(final int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }
    }
}
