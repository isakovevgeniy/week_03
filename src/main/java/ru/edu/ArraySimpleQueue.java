package ru.edu;

public class ArraySimpleQueue<T> implements SimpleQueue<T> {

    /**
     * ARRAY_LENGTH - длина массива.
     */
    private final int arrayLength;

    /**
     * array - это массив.
     */
    private Object[] array;

    /**
     * size - фактический размер/ количество элементов.
     */
    private int size;

    /**
     * @param length - длина списка.
     */
    public ArraySimpleQueue(final int length) {
        arrayLength = length;
        array = new Object[arrayLength];
    }

    /**
     * Добавление элемента в конец очереди.
     *
     * @param value элемент
     * @return true - если удалось поместить элемент (если есть место в очереди)
     */
    @Override
    public boolean offer(final Object value) {
        if (size == array.length) {
            return false;
        }
        array[size++] = value;
        return true;
    }

    /**
     * Получение и удаление первого элемента из очереди.
     *
     * @return первый элемент из очереди
     */
    @Override
    @SuppressWarnings("unchecked")
    public T poll() {
        Object obj;
        if (size == 0) {
            return null;
        } else {
            obj = array[0];
            for (int i = 1; i < size; i++) {
                array[i - 1] = array[i];
                if (array[i] == null) {
                    break;
                }
            }
            array[--size] = null;
        }
        return (T) obj;
    }

    /**
     * Получение БЕЗ удаления первого элемента из очереди.
     *
     * @return первый элемент из очереди
     */
    @Override
    @SuppressWarnings("unchecked")
    public T peek() {
        return (T) array[0];
    }

    /**
     * Количество элементов в очереди.
     *
     * @return количество элементов
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * Количество элементов которое может уместиться в очереди.
     *
     * @return -1 если не ограничено (будет расширяться),
     * либо конкретное число если ограничено.
     */
    @Override
    public int capacity() {
        return array.length;
    }
}
